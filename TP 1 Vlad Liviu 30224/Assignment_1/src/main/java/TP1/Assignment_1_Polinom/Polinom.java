package TP1.Assignment_1_Polinom;

import java.awt.List;
import java.util.ArrayList;
import java.util.Collections;


import java.util.ArrayList;
import java.util.Collections;

public class Polinom {

	private ArrayList<Monom> polinom = new ArrayList<Monom>();

	public void createPol(double coeficient, int exponent) {

		polinom.add(new Monom(coeficient, exponent));
		aranjare();
		Collections.sort(polinom);

	}

	public String toString() {
		if (polinom.size() == 0) {
			return "";
		} else {
			String s = "";

			for (int i = 0; i < polinom.size(); i++) {
				s += polinom.get(i).toString();
			}
			if (polinom.get(0).getCoeficient() > 1 || polinom.get(0).getCoeficient() == 1) {
				s = s.substring(1);
			}
			return s;
		}
	}

	public void addMonom(Monom m) {

		polinom.add(m);

	}

	public void aranjare() {
		Monom m;

		for (int i = 0; i < polinom.size() - 1; i++) {
			for (int j = i + 1; j < polinom.size(); j++) {
				if (polinom.get(i).getExponent() == polinom.get(j).getExponent()) {
					m = polinom.get(i).sum(polinom.get(j));
					polinom.add(m);
					polinom.remove(polinom.get(i));
					polinom.remove(polinom.get(j - 1));
				}
			}
		}

		for (int i = 0; i < polinom.size() - 1; i++) {
			if (this.polinom.get(i).getCoeficient() == 0) {
				this.polinom.remove(i);
			}
		}
		for (int i = 0; i < polinom.size() - 1; i++) {

			if (this.polinom.get(i).getCoeficient() == 0 && this.polinom.get(i).getExponent() == 0) {
				polinom.add(new Monom(0, 0));
			}
		}
	}

	public Polinom clone() {

		Polinom clone = new Polinom();
		for (Monom m : polinom) {

			clone.addMonom(m);
		}
		return clone;
	}

	public Polinom sum(Polinom p) {

		for (int i = 0; i < polinom.size(); i++) {
			p.createPol(polinom.get(i).getCoeficient(), polinom.get(i).getExponent());
		}
		p.aranjare();
		return p;
	}

	public Polinom sub(Polinom p) {
		Polinom rez = new Polinom();

		for (int i = 0; i < this.polinom.size(); i++) {
			rez.createPol(this.polinom.get(i).getCoeficient(), this.polinom.get(i).getExponent());
		}
		for (int i = 0; i < p.polinom.size(); i++) {
			rez.createPol(-p.polinom.get(i).getCoeficient(), p.polinom.get(i).getExponent());
		}

		rez.aranjare();
		return rez;
	}

	public Polinom mul(Polinom p) {

		Polinom rez = new Polinom();
		for (int i = 0; i < polinom.size(); i++) {
			for (int j = 0; j < p.polinom.size(); j++) {
				rez.createPol(polinom.get(i).getCoeficient() * p.polinom.get(j).getCoeficient(),
						polinom.get(i).getExponent() + p.polinom.get(j).getExponent());
			}
		}
		rez.aranjare();
		return rez;
	}

	public void derivare() {

		for (Monom m : polinom) {

			m.derivare();
		}
	}

	public void integrare() {

		for (Monom m : polinom) {
			m.integrare();

		}

	}

	public void deletePol() {

		polinom.clear();
	}

}
