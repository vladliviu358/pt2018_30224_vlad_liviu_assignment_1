package TP1.Assignment_1_Polinom;

public class Monom implements Comparable<Monom> {

	private double coeficient;
	private int exponent;

	public Monom(double coeficient, int exponent) {

		this.setCoeficient(coeficient);
		this.setExponent(exponent);
	}

	public double getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public int getExponent() {
		return exponent;
	}

	public void setExponent(int exponent) {
		this.exponent = exponent;
	}

	public void derivare() {

		coeficient = coeficient * exponent;
		exponent--;
	}


	public void integrare() {

		if (coeficient != 1) {
			coeficient = coeficient / (double) (exponent + 1);
			exponent++;
		} else {

		}
	}

	public int comparaExp(Monom m) {

		return m.getExponent() - this.exponent;
	}

	public Monom sum(Monom m) {

		double coef = 0;
		int exp = 0;
		if (this.exponent == m.exponent) {

			coef = this.coeficient + m.coeficient;
			exp = this.exponent;
		}

		return new Monom(coef, exp);
	}

	public boolean verifica(Monom m) {
		if (this.coeficient == 0 && this.exponent == 0)
			return true;
		else
			return false;
	}

	public String toString() {
		String s = "";
		double coef = this.coeficient;
		int exp = this.exponent;
		if (coef == 0) {
			s = "0";
		}

		if (coef > 0 && coef < 1) {
			s = "+";
			if (exp == 1)
				s += coef + "x";
			if (exp == 0)
				s += coef;
			if (exp != 0 && exp != 1)
				s += coef + "x^" + exp;
		}
		if (coef == 1) {
			if (exp == 1)
				s = "+1.0x^1";
			if (exp == 0)
				s = "+1.0x^0";
			if (exp != 0 && exp != 1)
				s = "+x^" + exp;
		}
		if (coef == -1) {
			if (exp == 1)
				s = "-1.0x^1";
			if (exp == 0)
				s = "-1.0x^0";
			if (exp != 0 && exp != 1)
				s = "-x^" + exp;
		}
		if (coef > 1) {
			s = "+";
			if (exp == 1)
				s += coef + "x^1";
			if (exp == 0)
				s += coef;
			if (exp != 0 && exp != 1)
				s += coef + "x^" + exp;
		}
		if (coef < -1) {
			if (exp == 1)
				s = coef + "x^1";
			if (exp == 0)
				s = Double.toString(coef);
			if (exp != 0 && exp != 1)
				s = coef + "x^" + exp;
		}

		return s;
	}

	public int compareTo(Monom m) {
		if (exponent > m.exponent) {
			return -1;
		}
		if (exponent == m.exponent) {
			return 0;
		}
		return 1;
	}
}
