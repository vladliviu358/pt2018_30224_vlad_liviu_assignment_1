package TP1.Assignment_1;

import static org.junit.Assert.*;

import org.junit.Test;

import TP1.Assignment_1_Polinom.Polinom;

public class PolinomSumTest {

	@Test
	public void testSum() {
		
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		p1.createPol(2, 2);
		p2.createPol(1, 2);
		p1.sum(p2);
		assertEquals("3.0x^2",p2.toString());
	}

}
