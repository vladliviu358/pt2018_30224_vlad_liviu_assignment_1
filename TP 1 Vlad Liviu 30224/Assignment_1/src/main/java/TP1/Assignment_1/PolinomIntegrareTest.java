package TP1.Assignment_1;

import static org.junit.Assert.*;

import org.junit.Test;

import TP1.Assignment_1_Polinom.Polinom;

public class PolinomIntegrareTest {

	@Test
	public void test() {
		
		Polinom p1 = new Polinom();
		p1.deletePol();
		p1.createPol(1, 2);
		p1.createPol(1, 1);
		p1.integrare();
		assertEquals("0.5x^3+0.5x^2",p1.toString());
	}

}
