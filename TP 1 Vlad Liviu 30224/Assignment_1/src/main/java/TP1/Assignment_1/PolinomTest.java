package TP1.Assignment_1;

import static org.junit.Assert.*;

import org.junit.Test;

import TP1.Assignment_1_Polinom.Polinom;

public class PolinomTest {

	@Test
	public void test() {
		
		Polinom p1 = new Polinom();
		p1.createPol(2, 2);
		p1.createPol(1, 3);
		p1.createPol(4, 1);
		p1.aranjare();
		assertEquals("x^3+2.0x^2+4.0x^1",p1.toString());
	}
	


}
