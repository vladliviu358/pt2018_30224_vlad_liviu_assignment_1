package TP1.Assignment_1;

import static org.junit.Assert.*;

import org.junit.Test;

import TP1.Assignment_1_Polinom.Polinom;

public class PolinomMulTest {

	@Test
	public void testMul() {
		
		Polinom p1 = new Polinom();
		Polinom p2 = new Polinom();
		Polinom rez = new Polinom();
		p1.createPol(2, 2);
		p2.createPol(1, 2);
		rez = p1.mul(p2);
		assertEquals("2.0x^4",rez.toString());
	}

}
