package TP1.Assignment_1;

import javax.swing.JFrame;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionEvent;
import java.awt.Color;





import javax.swing.JTextField;
import javax.swing.JLabel;

import TP1.Assignment_1_Polinom.Polinom;

import java.awt.Font;


public class Window {

	public JFrame frame;
	private JTextField polinom1;
	private JTextField polinom2;
	private JTextField rezultat;
	private Polinom p1 = new Polinom();
	private Polinom p2 = new Polinom();
	private Polinom rez = new Polinom();

	// public static void main(String[] args) {
	// EventQueue.invokeLater(new Runnable() {
	// public void run() {
	// try {
	// Window window = new Window();
	// window.frame.setVisible(true);
	// } catch (Exception e) {
	// e.printStackTrace();
	// }
	// }
	// });
	// }

	public Window() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(new Color(255, 204, 153));
		frame.setBackground(new Color(139, 69, 19));
		frame.setTitle("Calculator Polinomial");
		frame.setBounds(100, 100, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblNewLabel = new JLabel("Polinom 1");
		lblNewLabel.setFont(new Font("Times New Roman", lblNewLabel.getFont().getStyle(), 22));
		lblNewLabel.setBounds(114, 82, 120, 35);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblPolinom = new JLabel("Polinom 2");
		lblPolinom.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblPolinom.setBounds(114, 130, 120, 35);
		frame.getContentPane().add(lblPolinom);

		polinom1 = new JTextField();
		polinom1.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		polinom1.setBounds(256, 82, 185, 35);
		frame.getContentPane().add(polinom1);
		polinom1.setColumns(10);

		polinom2 = new JTextField();
		polinom2.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		polinom2.setColumns(10);
		polinom2.setBounds(256, 130, 185, 35);
		frame.getContentPane().add(polinom2);

		JLabel lblRezultat = new JLabel(" Rezultat");
		lblRezultat.setFont(new Font("Times New Roman", Font.BOLD, 22));
		lblRezultat.setBounds(114, 178, 120, 35);
		frame.getContentPane().add(lblRezultat);

		rezultat = new JTextField();
		rezultat.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		rezultat.setColumns(10);
		rezultat.setBounds(256, 178, 185, 35);
		frame.getContentPane().add(rezultat);

		JButton btnAdunare = new JButton("Adunare");
		btnAdunare.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				String input = polinom1.getText();
				Pattern p = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(input);
				while (m.find()) {
					p1.createPol(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
					System.out.println("Coef: " + m.group(1));
					System.out.println("Degree: " + m.group(2));
				}
				String input2 = polinom2.getText();
				Pattern pat1 = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m1 = pat1.matcher(input2);
				while (m1.find()) {
					p1.createPol(Double.parseDouble(m1.group(1)), Integer.parseInt(m1.group(2)));
					System.out.println("Coef: " + m1.group(1));
					System.out.println("Degree: " + m1.group(2));
				}

				p1.sum(p2);
				rezultat.setText(p2.toString());
				p1.deletePol();
				p2.deletePol();
				rez.deletePol();

			}
		});
		btnAdunare.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnAdunare.setBounds(114, 286, 120, 35);
		frame.getContentPane().add(btnAdunare);

		JButton btnScadere = new JButton("Scadere");
		btnScadere.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnScadere.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Polinom p3 = new Polinom();
				Polinom p4 = new Polinom();
				String input = polinom1.getText();
				Pattern p = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(input);
				while (m.find()) {
					p3.createPol(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
					System.out.println("Coef: " + m.group(1));
					System.out.println("Degree: " + m.group(2));
				}
				String input2 = polinom2.getText();
				Pattern pat1 = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m1 = pat1.matcher(input2);
				while (m1.find()) {
					p4.createPol(Double.parseDouble(m1.group(1)), Integer.parseInt(m1.group(2)));
					System.out.println("Coef: " + m1.group(1));
					System.out.println("Degree: " + m1.group(2));
				}

				rez = p3.sub(p4);
				rezultat.setText(rez.toString());
				System.out.println(rez.toString());
				p3.deletePol();
				p4.deletePol();
				rez.deletePol();
			}
		});
		btnScadere.setBounds(114, 341, 120, 35);
		frame.getContentPane().add(btnScadere);

		JButton btnInmultire = new JButton("Inmultire");
		btnInmultire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Polinom p5 = new Polinom();
				Polinom p6 = new Polinom();

				String input = polinom1.getText();
				Pattern p = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(input);
				while (m.find()) {
					p5.createPol(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
					System.out.println("Coef: " + m.group(1));
					System.out.println("Degree: " + m.group(2));
				}
				String input2 = polinom2.getText();
				Pattern pat1 = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m1 = pat1.matcher(input2);
				while (m1.find()) {
					p6.createPol(Double.parseDouble(m1.group(1)), Integer.parseInt(m1.group(2)));
					System.out.println("Coef: " + m1.group(1));
					System.out.println("Degree: " + m1.group(2));
				}
				rez = p5.mul(p6);
				rezultat.setText(rez.toString());
				System.out.println(rez.toString());
				p5.deletePol();
				p6.deletePol();

			}
		});
		btnInmultire.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnInmultire.setBounds(299, 286, 120, 35);
		frame.getContentPane().add(btnInmultire);

		JButton btnImpartire = new JButton("Impartire\r\n");
		btnImpartire.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				rezultat.setText("Yet to be coded");
			}
		});
		btnImpartire.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnImpartire.setBounds(299, 341, 120, 35);
		frame.getContentPane().add(btnImpartire);

		JButton btnDerivareP = new JButton("Derivare P1");
		btnDerivareP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Polinom p7 = new Polinom();
				String input = polinom1.getText();
				Pattern p = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(input);
				while (m.find()) {
					p7.createPol(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
					System.out.println("Coef: " + m.group(1));
					System.out.println("Degree: " + m.group(2));
				}
				p7.derivare();
				rezultat.setText(p7.toString());
				p7.deletePol();
			}
		});
		btnDerivareP.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnDerivareP.setBounds(468, 286, 136, 35);
		frame.getContentPane().add(btnDerivareP);

		JButton btnIntegrareP = new JButton("Integrare P1\r\n");
		btnIntegrareP.setFont(new Font("Times New Roman", Font.PLAIN, 18));
		btnIntegrareP.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				Polinom p9 = new Polinom();
				String input = polinom1.getText();
				Pattern p = Pattern.compile("(-?\\b\\d\\.\\d+)[xX]\\^(-?\\d+\\b)");
				Matcher m = p.matcher(input);
				while (m.find()) {
					p9.createPol(Double.parseDouble(m.group(1)), Integer.parseInt(m.group(2)));
					System.out.println("Coef: " + m.group(1));
					System.out.println("Degree: " + m.group(2));
				}
				p9.integrare();
				rezultat.setText(p9.toString());
				p9.deletePol();
			}
		});
		btnIntegrareP.setBounds(468, 341, 136, 35);
		frame.getContentPane().add(btnIntegrareP);

		JButton clear1 = new JButton("X");
		clear1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				polinom1.setText("");

			}
		});
		clear1.setBounds(453, 82, 46, 35);
		frame.getContentPane().add(clear1);

		JButton clear2 = new JButton("X");
		clear2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				polinom2.setText("");
			}
		});
		clear2.setBounds(453, 130, 46, 35);
		frame.getContentPane().add(clear2);
	}
}
